# Zenvia - Caixa Eletrônico

# Desafio Zenvia - Caixa Eletrônico

Este projeto teve o intuito de construir uma aplicação para fornecer a funcionalidade de saque para um caixa eletrônico.

Para maiores informações sobre o projeto do desafio, entrar no link a seguir http://dojopuzzles.com/problemas/exibe/caixa-eletronico/

Algumas das tecnologias utilizadas no desenvolvimento foram:

* Java 11 com Spring-Boot
* MySql
* Lombok
* FlyWay
* Docker
* Swagger

Seguindo os conceitos da arquitetura em camadas e delegando apenas as responsabilidades necessárias para cada classe do sistema.

## Instruções de execução

### Requisitos
* docker (https://www.docker.com/get-started)
* docker-compose

### Executar a aplicação

Na raiz do projeto, executar o seguinte comando para realizar a criação do banco de dados e subir o container da aplicação:

````
docker-compose -f docker-compose.yml up -d
````

Para verificar se o container está funcional, basta executar:

````
docker ps
````

Com o container em execução podemos agora iniciar a nossa aplicação do backend com os seguintes comandos na raiz do projeto:

````
./mvnw clean package

./mvnw spring-boot:run
````

À partir deste momento será iniciado a nossa aplicação e o Flyway irá criar a tabela `banknotes` automaticamente no banco de dados com os dados que serão utilizados pela aplicação.

Para acessar os Endpoints pelo Swagger, acessar o endereço abaixo:

`` http://localhost:8080//swagger-ui.html ``

Para encerrar o container do docker, executar o seguinte comando:

`` sudo docker-compose down ``

## End-points

* `` POST http://localhost:8080/api/v1/cashmachine `` - realiza uma requisição de saque com o valor solicitado. Primeiramente o valor passa por uma validação para verificar se é maior ou igual que R$ 10,00, caso positivo será realizado o calculo para retornar o menor número possível de notas para o usuário.
````
{
    "value": 480
}
````
Retorno esperado:
````
[
  {
    "value": 100,
    "quantity": 4
  },
  {
    "value": 50,
    "quantity": 1
  },
  {
    "value": 20,
    "quantity": 1
  },
  {
    "value": 10,
    "quantity": 1
  }
]
````

* `` GET http://localhost:8080/api/v1/cashmachine `` - retornará todas as cédulas inseridas no banco de dados.

