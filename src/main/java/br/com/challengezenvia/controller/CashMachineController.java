package br.com.challengezenvia.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.challengezenvia.model.dto.BanknotesResponseDto;
import br.com.challengezenvia.model.dto.WithdrawRequestDto;
import br.com.challengezenvia.model.entity.Banknotes;
import br.com.challengezenvia.service.BanknoteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "api/v1/cashmachine")
public class CashMachineController {
	
	@Autowired
	private BanknoteService banknoteService;

	@ApiOperation("Find all banknotes.")
	@ApiResponses(value = { @ApiResponse(code = 202, message = "Found banknotes."),
			@ApiResponse(code = 404, message = "Banknotes not found.") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Banknotes>> getAllBanknotes() {
		
		List<Banknotes> result = banknoteService.findAll();
		if (result != null) {
			return new ResponseEntity<List<Banknotes>>(result, HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<List<Banknotes>>(HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation("Make a withdrawal amount.")
	@ApiResponses(value = { @ApiResponse(code = 202, message = "Withdrawal made."),
			@ApiResponse(code = 400, message = "Invalid withdrawal amount.") })
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<BanknotesResponseDto>> withdraw(@RequestBody @Valid WithdrawRequestDto withdrawRequest) {
		
		if (withdrawRequest.getValue() % 10 != 0) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		List<BanknotesResponseDto> result = banknoteService.withdraw(withdrawRequest.getValue());
		return new ResponseEntity<List<BanknotesResponseDto>>(result, HttpStatus.ACCEPTED);
	}
}
