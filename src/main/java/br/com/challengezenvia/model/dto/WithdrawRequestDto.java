package br.com.challengezenvia.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawRequestDto {
	
	@NotNull(message="Essa propriedade é obrigatória")
	@Min(value = 10)
	private Integer value;
}
