package br.com.challengezenvia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.challengezenvia.model.entity.Banknotes;

@Repository
public interface BanknoteRepository extends JpaRepository<Banknotes, Long> {

}