package br.com.challengezenvia.service;

import java.util.List;

import br.com.challengezenvia.model.dto.BanknotesResponseDto;
import br.com.challengezenvia.model.entity.Banknotes;

public interface BanknoteService {
	
	List<Banknotes> findAll();
	List<BanknotesResponseDto> withdraw(Integer value);
}
