package br.com.challengezenvia.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.challengezenvia.model.dto.BanknotesResponseDto;
import br.com.challengezenvia.model.entity.Banknotes;
import br.com.challengezenvia.repository.BanknoteRepository;
import br.com.challengezenvia.service.BanknoteService;

@Service
public class BanknoteServiceImpl implements BanknoteService {
	
	private final BanknoteRepository banknoteRepository;
	
	@Autowired
	public BanknoteServiceImpl(final BanknoteRepository banknoteRepository) {
		this.banknoteRepository = banknoteRepository;
	}

	@Override
	public List<Banknotes> findAll() {
		return banknoteRepository.findAll();
	}
	
	@Override
	public List<BanknotesResponseDto> withdraw(Integer value) {
		
		List<BanknotesResponseDto> banknotesResponse = new ArrayList<BanknotesResponseDto>();
		List<Banknotes> listBanknotes = banknoteRepository.findAll();
		
		for (int i = 0; i < listBanknotes.size(); i++) {
			if (value >= listBanknotes.get(i).getValue()) {
				
				BanknotesResponseDto banknotesResponseDto = new BanknotesResponseDto();
				banknotesResponseDto.setValue(listBanknotes.get(i).getValue());
				banknotesResponseDto.setQuantity(Integer.divideUnsigned(value, listBanknotes.get(i).getValue()));
				
				value = (value-(banknotesResponseDto.getValue()*banknotesResponseDto.getQuantity()));
				banknotesResponse.add(banknotesResponseDto);
			}
		}
		
		return banknotesResponse;
	}
}
