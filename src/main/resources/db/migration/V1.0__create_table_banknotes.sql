USE app_development;

CREATE TABLE banknotes (
    id bigint NOT NULL AUTO_INCREMENT,
    value integer(3) NOT NULL,
    quantity integer NOT NULL,
    created_at timestamp not null DEFAULT CURRENT_TIMESTAMP,
	updated_at timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
);

SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;

INSERT INTO banknotes (value, quantity) VALUES(100, 999999);
INSERT INTO banknotes (value, quantity) VALUES(50, 999999);
INSERT INTO banknotes (value, quantity) VALUES(20, 999999);
INSERT INTO banknotes (value, quantity) VALUES(10, 999999);
