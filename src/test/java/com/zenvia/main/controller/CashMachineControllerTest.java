package com.zenvia.main.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.challengezenvia.controller.CashMachineController;
import br.com.challengezenvia.model.dto.BanknotesResponseDto;
import br.com.challengezenvia.model.dto.WithdrawRequestDto;
import br.com.challengezenvia.model.entity.Banknotes;
import br.com.challengezenvia.service.BanknoteService;

@RunWith(SpringRunner.class)
public class CashMachineControllerTest {
	
	@Mock
	BanknoteService banknoteService;
	
	@InjectMocks
	CashMachineController cashMachineController;
	
	@Test
	public void getAllBanknotesFindAllSuccess() {
		List<Banknotes> banknotesList = List.of(new Banknotes(1L, 50, 1, LocalDateTime.now(), LocalDateTime.now()));
		
		when(banknoteService.findAll()).thenReturn(banknotesList);
		var result = cashMachineController.getAllBanknotes();
		
		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
		assertEquals(banknotesList, result.getBody());
	}
	
	@Test
	public void getAllBanknotesFindAllNotFound() {
		when(banknoteService.findAll()).thenReturn(null);
		var result = cashMachineController.getAllBanknotes();
		
		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
	}
	
	@Test
	public void withdrawBadRequest() {
		WithdrawRequestDto withdrawRequest = new WithdrawRequestDto(1354);
		
		var result = cashMachineController.withdraw(withdrawRequest);
		
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}
	
	@Test
	public void withdrawSuccess() {
		WithdrawRequestDto withdrawRequest = new WithdrawRequestDto(50);
		List<BanknotesResponseDto> listBanknotesResponseDto = List.of(new BanknotesResponseDto(50, 1));
		
		when(banknoteService.withdraw(withdrawRequest.getValue())).thenReturn(listBanknotesResponseDto);
		var result = cashMachineController.withdraw(withdrawRequest);
		
		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
		assertEquals(listBanknotesResponseDto, result.getBody());
	}
}