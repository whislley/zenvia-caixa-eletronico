package com.zenvia.main.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.challengezenvia.model.dto.BanknotesResponseDto;
import br.com.challengezenvia.model.entity.Banknotes;
import br.com.challengezenvia.repository.BanknoteRepository;
import br.com.challengezenvia.service.BanknoteService;
import br.com.challengezenvia.service.impl.BanknoteServiceImpl;

@RunWith(SpringRunner.class)
public class BanknoteServiceTest {
	
	@Mock
	BanknoteRepository banknoteRepository;
	
	@Test()
	public void findAll() {
		List<Banknotes> banknotesList = List.of(new Banknotes());
		
		when(banknoteRepository.findAll()).thenReturn(banknotesList);
		BanknoteService banknoteService = new BanknoteServiceImpl(banknoteRepository);
		List<Banknotes> banknotesListFromDataBase = banknoteService.findAll();
		
		assertEquals(banknotesListFromDataBase, banknotesList);
	};
	
	@Test()
	public void withdraw() {
		Banknotes banknote100 = new Banknotes(1L, 100, 999999, LocalDateTime.now(), LocalDateTime.now());
		Banknotes banknote50 = new Banknotes(1L, 50, 999999, LocalDateTime.now(), LocalDateTime.now());
		Banknotes banknote20 = new Banknotes(1L, 20, 999999, LocalDateTime.now(), LocalDateTime.now());
		Banknotes banknote10 = new Banknotes(1L, 10, 999999, LocalDateTime.now(), LocalDateTime.now());
		List<Banknotes> banknotesList = List.of(banknote100, banknote50, banknote20, banknote10);
		Integer value = 50;
		List<BanknotesResponseDto> banknotesResponseDtoList = List.of(new BanknotesResponseDto(50, 1));
		
		when(banknoteRepository.findAll()).thenReturn(banknotesList);
		BanknoteService banknoteService = new BanknoteServiceImpl(banknoteRepository);
		List<BanknotesResponseDto> result = banknoteService.withdraw(value);
		
		assertEquals(banknotesResponseDtoList, result);
	};

}
